//
//  FinanceOffice.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import Foundation
import RealmSwift

final class FinanceOffice: Object, IdentifiableObject, Decodable {

    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var zip: String = ""
    @objc dynamic var city: String = ""
    @objc dynamic var street: String = ""
    @objc dynamic var phone: String = ""
    @objc dynamic var openingHours: String = ""
    @objc dynamic var imageURL: String = ""
    @objc dynamic var latitude: String = ""
    @objc dynamic var longitude: String = ""

    override static func primaryKey() -> String? {
        return "id"
    }

    // MARK: - Coding Keys

    enum CodingKeys: String, CodingKey {
        case id = "DisKz"
        case name = "DisNameLang"
        case zip = "DisPlz"
        case city = "DisOrt"
        case street = "DisStrasse"
        case phone = "DisTel"
        case openingHours = "DisOeffnung"
        case imageURL = "DisFotoUrl"
        case latitude = "DisLatitude"
        case longitude = "DisLongitude"
    }
}
