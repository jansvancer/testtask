//
//  IdentifiableObject.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import Foundation

protocol IdentifiableObject {
    var id: String { get }
}
