//
//  ViewModelAttaching.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit

protocol ViewModelAttaching: class {
    associatedtype ViewModel: ViewModelType

    var bindings: ViewModel.Bindings { get }
    var viewModel: ViewModel! { get set }

    func attach(wrapper: ViewModelWrapper<ViewModel>) -> ViewModel
    func setupUI()
    func addSubviews()
    func layout()
    func setupNavigationBar()
    func setupBindings(viewModel: ViewModel)

    static func create() -> Self
}

extension ViewModelAttaching where Self: UIViewController {

    @discardableResult
    func attach(wrapper: ViewModelWrapper<ViewModel>) -> ViewModel {
        viewModel = wrapper.bind(bindings)
        loadViewIfNeeded()
        setupUI()
        addSubviews()
        layout()
        setupNavigationBar()
        setupBindings(viewModel: viewModel)
        return viewModel
    }

    func addSubviews() {}

    func layout() {}

    func setupNavigationBar() {}

    func setupBindings(viewModel: ViewModel) {}

    static func create() -> Self {
        return self.init()
    }
}
