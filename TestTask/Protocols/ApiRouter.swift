//
//  ApiRouter.swift
//  GulfBrokers
//
//  Created by Jan Švancer on 04/02/2019.
//  Copyright © 2019 CLEEVIO s.r.o. All rights reserved.
//

import Foundation
import Alamofire

protocol ApiRouter: URLRequestConvertible {
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters { get }
}
