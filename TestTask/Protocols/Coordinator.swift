//
//  Coordinator.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import Foundation
import RxSwift

enum PushCoordinationResult<T> {
    case popped
    case dismiss
    case finished(T)
}

protocol Coordinator {
    associatedtype CoordinationResult

    var identifier: UUID { get }

    func start() -> Observable<CoordinationResult>
}
