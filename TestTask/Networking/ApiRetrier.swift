//
//  ApiRetrier.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import KeychainAccess

final class ApiRetrier: RequestRetrier {

    let disposeBag = DisposeBag()

    private let lock = NSLock()
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []

    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock(); defer { lock.unlock() }

        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == ApiService.StatusCode.unauthorized {
            requestsToRetry.append(completion)

            if !isRefreshing {
                // TODO: refresh authorization
            }
        } else {
            completion(false, 0.0)
        }
    }
}
