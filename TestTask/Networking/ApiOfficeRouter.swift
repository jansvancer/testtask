//
//  ApiOfficeRouter.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import Foundation
import Alamofire

enum ApiOfficeRouter: ApiRouter {

    // MARK: - Endpoints

    case financeOffices

    // MARK: - Methods

    var method: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }

    // MARK: - Paths

    var path: String {
        switch self {
        case .financeOffices:
            return "/Finanzamtsliste.json"
        }
    }

    // MARK: - Parameters

    var parameters: Parameters {
        switch self {
        default:
            return [:]
        }
    }

    // MARK: - URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let urlPath = "\(Constants.API.baseURLString)\(path)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        var urlRequest = URLRequest(url: try urlPath.asURL())

        urlRequest.httpMethod = method.rawValue

        return urlRequest
    }
}
