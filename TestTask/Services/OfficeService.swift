//
//  OfficeService.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import Foundation
import RxSwift

final class OfficeService: BaseService { }

// MARK: - Office Service

protocol OfficeServiceType {
    func updateOffices() -> Single<[FinanceOffice]>
    func listAllOffices() -> Observable<[FinanceOffice]>
    func office(for id: String) -> Observable<FinanceOffice>
}

extension OfficeService: OfficeServiceType {
    func updateOffices() -> Single<[FinanceOffice]> {
        return request(type: [FinanceOffice].self, endpoint: ApiOfficeRouter.financeOffices)
            .do(onSuccess: { [unowned self] financeOffices in
                self.realmManager.write(collection: financeOffices, deleteDifferences: true)
            })
    }

    func listAllOffices() -> Observable<[FinanceOffice]> {
        let objects = realmManager.realm.objects(FinanceOffice.self)

        return Observable.array(from: objects.sorted(byKeyPath: #keyPath(FinanceOffice.zip)))
    }

    func office(for id: String) -> Observable<FinanceOffice> {
        return realmManager.writeSync { realm in
            let object = realm.findOrCreate(uuid: id, of: FinanceOffice.self)
            return Observable.from(object: object)
        }
    }
}
