//
//  RealmManager.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxRealm

final class RealmManager {

    let realm: Realm

    init() {
        Realm.Configuration.defaultConfiguration = Realm.Configuration(
            schemaVersion: 1
        )

        self.realm = RealmManager.loadRealm()

        #if DEBUG
        log.debug(Realm.Configuration.defaultConfiguration.fileURL!.absoluteString)
        #endif
    }
}

// MARK: - Realm Service

protocol RealmService {
    func observeCollection<T: Object>(object: T.Type, synchronousStart: Bool) -> Observable<Results<T>>
    func observeArray<T: Object>(object: T.Type, synchronousStart: Bool) -> Observable<[T]>
    func observeChangeset<T: Object>(object: T.Type, synchronousStart: Bool) -> Observable<(AnyRealmCollection<T>, RealmChangeset?)>
    func observeArrayChangeset<T: Object>(object: T.Type, synchronousStart: Bool) -> Observable<([T], RealmChangeset?)>

    func writeSync<T>(_ block: @escaping (Realm) -> T) -> T
    func write<T: Object>(collection: [T], deleteDifferences: Bool, update: Realm.UpdatePolicy, predicate: NSPredicate?)
    func writeBackground<T: Object>(collection: [T], deleteDifferences: Bool, update: Realm.UpdatePolicy, predicate: NSPredicate?)
    func writeBackground<T>(_ background: @escaping (Realm) throws -> T) -> Single<T>
    func deleteAll()
}

extension RealmManager: RealmService {
    func observeCollection<T: Object>(object: T.Type, synchronousStart: Bool = true) -> Observable<Results<T>> {
        let object = realm.objects(T.self)

        return Observable.collection(from: object, synchronousStart: synchronousStart)
    }

    func observeArray<T: Object>(object: T.Type, synchronousStart: Bool = true) -> Observable<[T]> {
        let object = realm.objects(T.self)

        return Observable.array(from: object, synchronousStart: synchronousStart)
    }

    func observeChangeset<T: Object>(object: T.Type, synchronousStart: Bool = true) -> Observable<(AnyRealmCollection<T>, RealmChangeset?)> {
        let object = realm.objects(T.self)

        return Observable.changeset(from: object, synchronousStart: synchronousStart)
    }

    func observeArrayChangeset<T: Object>(object: T.Type, synchronousStart: Bool = true) -> Observable<([T], RealmChangeset?)> {
        let object = realm.objects(T.self)

        return Observable.arrayWithChangeset(from: object, synchronousStart: synchronousStart)
    }

    func writeSync<T>(_ block: @escaping (Realm) -> T) -> T {
        var value: T?

        try? realm.write {
            value = block(realm)
        }

        return value!
    }

    func write<T: Object>(collection: [T], deleteDifferences: Bool, update: Realm.UpdatePolicy = .all, predicate: NSPredicate? = nil) {
        writeSync { realm in
            var collectionUuids: [String] = []
            collection.forEach { item in
                realm.add(item, update: update)

                if let primaryKey = T.primaryKey(), let primaryKeyValue = item.value(forKey: primaryKey) as? String {
                    collectionUuids.append(primaryKeyValue)
                }
            }

            if deleteDifferences, let primaryKey = T.primaryKey() {
                let idsPredicate = NSPredicate(format: "NOT \(primaryKey) IN %@", collectionUuids)
                let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [idsPredicate, predicate].compactMap { $0 })

                realm.delete(realm.objects(T.self).filter(predicate))
            }
        }
    }

    func writeBackground<T: Object>(collection: [T], deleteDifferences: Bool, update: Realm.UpdatePolicy = .all, predicate: NSPredicate? = nil) {
        do {
            let backgroundRealm = RealmManager.loadRealm()
            try backgroundRealm.write {
                var collectionUuids: [String] = []
                collection.forEach { item in
                    backgroundRealm.add(item, update: update)

                    if let primaryKey = T.primaryKey(), let primaryKeyValue = item.value(forKey: primaryKey) as? String {
                        collectionUuids.append(primaryKeyValue)
                    }
                }

                if deleteDifferences, let primaryKey = T.primaryKey() {
                    let idsPredicate = NSPredicate(format: "NOT \(primaryKey) IN %@", collectionUuids)
                    let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [idsPredicate, predicate].compactMap { $0 })

                    backgroundRealm.delete(backgroundRealm.objects(T.self).filter(predicate))
                }
            }
        } catch {
            log.error(error.localizedDescription)
        }
    }

    func writeBackground<T>(_ background: @escaping (Realm) throws -> T) -> Single<T> {
        return Single.create { single in
            DispatchQueue.global(qos: .background).async {
                do {
                    var value: T?

                    let backgroundRealm = RealmManager.loadRealm()
                    try backgroundRealm.write {
                        value = try background(backgroundRealm)
                    }

                    single(.success(value!))
                } catch {
                    single(.error(error))
                }
            }

            return Disposables.create()
        }
    }

    func deleteAll() {
        do {
            try realm.write {
                realm.deleteAll()
            }
        } catch {
            assert(false, "Realm delete all error: \(error)")
        }
    }

    func delete<T: Object>(objects: T.Type) {
        do {
            try realm.write {
                realm.delete(realm.objects(objects))
            }
        } catch {
            assert(false, "Realm delete all error: \(error)")
        }
    }

}

// MARK: - Create realm

extension RealmManager {

    static func loadRealm() -> Realm {
        do {
            return try RealmProvider.realm()
        } catch Realm.Error.schemaMismatch {
            return RealmManager.handleRealmSchemaMismatch()
        } catch {
            fatalError("Error while loading Realm")
        }
    }

    static private func handleRealmSchemaMismatch() -> Realm {
        #if DEBUG
        log.warning("\n\n!!! DEBUG !!! Deleting old version of realm because of Realm.Error.schemaMismatch\n\n")

        do {
            try FileManager.default.removeItem(at: Realm.Configuration.defaultConfiguration.fileURL!)
        } catch {
            fatalError("!!! DEBUG !!! Error while removing old realm file")
        }

        do {
            return try RealmProvider.realm()
        } catch {
            fatalError("!!! DEBUG !!! Error while loading Realm")
        }
        #else
        fatalError("Error while loading Realm - Realm.Error.schemaMismatch")
        #endif
    }
}

// MARK: - RealmProvider

final class RealmProvider {
    static func realm() throws -> Realm {
        if NSClassFromString("XCTest") != nil {
            return try Realm(configuration: Realm.Configuration(fileURL: nil,
                                                                inMemoryIdentifier: "inMemoryDB",
                                                                encryptionKey: nil,
                                                                readOnly: false,
                                                                schemaVersion: 0,
                                                                migrationBlock: nil,
                                                                objectTypes: nil))
        } else {
            return try Realm()

        }
    }
}
