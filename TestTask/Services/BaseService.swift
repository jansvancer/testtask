//
//  BaseService.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import Foundation
import RxSwift

enum ServiceError: Error {
    case serializationError(message: String)
}

class BaseService {

    let apiService: ApiService
    let realmManager: RealmManager
    let disposeBag = DisposeBag()

    init(apiService: ApiService, realmManager: RealmManager) {
        self.apiService = apiService
        self.realmManager = realmManager
    }

    // MARK: - Requests

    func request<T: Decodable>(type: T.Type, endpoint: ApiRouter, scheduler: ConcurrentDispatchQueueScheduler? = nil) -> Single<T> {
        var request = apiService.request(endpoint: endpoint)

        if let scheduler = scheduler {
            request = request.observeOn(scheduler)
        }

        return request
            .map { [unowned self] data -> T in
                try self.serialize(data: data, toObject: T.self)
            }
            .asSingle()
    }

    func request(endpoint: ApiRouter) -> Single<Void> {
        return apiService.voidRequest(endpoint: endpoint)
            .asSingle()
    }

    // MARK: - Serializer

    private func serialize<T: Decodable>(data: Data, toObject: T.Type) throws -> T {
        guard let utfData = String(data: data, encoding: .ascii)?.data(using: .utf8) else {
            throw ServiceError.serializationError(message: "Unable to convert data to UTF8")
        }

        //let json = try JSONSerialization.jsonObject(with: utfData, options: .allowFragments)
        //guard (json as? [String: Any]) != nil else {
        //    throw ServiceError.serializationError(message: "Failed to parse JSON")
        //}

        return try T(data: utfData)
    }
}
