//
//  String+Extensions.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import Foundation

extension String {

    func removeWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}
