//
//  Keychain+Extensions.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import Foundation
import KeychainAccess

extension Keychain {
    static let standard = Keychain(service: UIDevice.targetName!.removeWhitespaces())

    subscript(key: Constants.KeychainKeys) -> String? {
        get {
            return self[key.rawValue]
        }
        set {
            self[key.rawValue] = newValue
        }
    }

    subscript(data key: Constants.KeychainKeys) -> Data? {
        get {
            return self[data: key.rawValue]
        }
        set {
            self[data: key.rawValue] = newValue
        }
    }
}
