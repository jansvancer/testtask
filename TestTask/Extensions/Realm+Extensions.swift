//
//  Realm+Extensions.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import Foundation
import RealmSwift

extension Realm {
    func findOrCreate<T: Object>(uuid: String, of: T.Type = T.self) -> T where T: IdentifiableObject {
        return self.create(T.self, value: ["id": uuid], update: .modified)
    }
}
