//
//  Rx+Extensions.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

// Originally from here:
// https://github.com/artsy/eidolon/blob/24e36a69bbafb4ef6dbe4d98b575ceb4e1d8345f/Kiosk/Observable%2BOperators.swift#L30-L40
// Credit to Artsy and @ashfurrow
public protocol _OptionalType {
    associatedtype Wrapped

    var value: Wrapped? { get }
}

extension Optional: _OptionalType {
    /// Cast `Optional<Wrapped>` to `Wrapped?`
    public var value: Wrapped? {
        return self
    }
}

// Some code originally from here:
// https://github.com/artsy/eidolon/blob/24e36a69bbafb4ef6dbe4d98b575ceb4e1d8345f/Kiosk/Observable%2BOperators.swift#L42-L62
// Credit to Artsy and @ashfurrow
public extension ObservableType where Element: _OptionalType {
    /**
     Unwraps and filters out `nil` elements.
     - returns: `Observable` of source `Observable`'s elements, with `nil` elements filtered out.
     */
    func filterNil() -> Observable<Element.Wrapped> {
        return self.flatMap { element -> Observable<Element.Wrapped> in
            guard let value = element.value else {
                return Observable<Element.Wrapped>.empty()
            }
            return Observable<Element.Wrapped>.just(value)
        }
    }
}

extension SharedSequenceConvertibleType where SharingStrategy == DriverSharingStrategy, Element: _OptionalType {
    public func filterNil() -> Driver<Element.Wrapped> {
        return self.flatMap { element -> Driver<Element.Wrapped> in
            guard let value = element.value else {
                return Driver<Element.Wrapped>.empty()
            }
            return Driver<Element.Wrapped>.just(value)
        }
    }
}

extension SharedSequence {
    var asVoid: SharedSequence<SharingStrategy, Void> {
        return map { _ in }
    }

    var asOptional: SharedSequence<SharingStrategy, Element?> {
        return map { value -> Element? in value }
    }
}

extension PrimitiveSequence where Trait == SingleTrait {

    var asVoid: PrimitiveSequence<Trait, Void> {
        return map { _ in }
    }

    var asOptional: PrimitiveSequence<Trait, Element?> {
        return map { value -> Element? in value }
    }

    func catchErrorJustReturnNil() -> PrimitiveSequence<Trait, Element?> {
        return asOptional.catchErrorJustReturn(nil)
    }
}

extension ObservableType {
    var asVoid: RxSwift.Observable<Void> {
        return map { _ in }
    }

    var asOptional: RxSwift.Observable<Element?> {
        return map { value -> Element? in value }
    }

    func catchErrorJustReturnNil() -> Observable<Element?> {
        return asOptional.catchErrorJustReturn(nil)
    }

    func catchErrorJustComplete() -> Observable<Element> {
        return catchError { _ in
            Observable.empty()
        }
    }

    func asDriverOnErrorJustComplete() -> Driver<Element> {
        return asDriver { _ in
            Driver.empty()
        }
    }

    func asDriverOnErrorJustReturnNil() -> Driver<Element?> {
        return asOptional.asDriver(onErrorJustReturn: nil)
    }

    func asDriverFilterNil() -> Driver<Element> {
        return asDriverOnErrorJustReturnNil().filterNil()
    }
}

extension ObservableType where Element == Bool {
    /// Boolean not operator
    public func not() -> Observable<Bool> {
        return map(!)
    }
}

extension SharedSequenceConvertibleType where Element == Bool {
    /// Boolean not operator.
    public func not() -> SharedSequence<SharingStrategy, Bool> {
        return map(!)
    }
}

// MARK: - UIViewController

extension Reactive where Base: UIViewController {

    public var activityIndicator: Binder<Bool> {
        return Binder(self.base) { viewController, isActive in
            isActive ? viewController.startActivity() : viewController.stopActivity()
        }
    }

    public var errors: Binder<Error> {
        return Binder(self.base) { viewController, error in
            log.info("‼️ ERROR: \((error as NSError).debugDescription) ‼️")
            viewController.handleError(error: error)
        }
    }
}

// MARK: - TableView

extension Reactive where Base: UITableView {
    public var reloadData: Binder<Void> {
        return Binder(self.base) { tableView, _ in
            tableView.reloadData()
        }
    }

    public var footerActivityIndicator: Binder<Bool> {
        return Binder(self.base) { tableView, isActive in
            isActive ? tableView.showLoadingFooter() : tableView.hideLoadingFooter()
        }
    }
}
