//
//  UITableView+Extensions.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit

extension UITableView {
    func showLoadingFooter() {
        let footerView = UIActivityIndicatorView(style: .gray)
        footerView.frame.size.height = 50
        footerView.hidesWhenStopped = true
        footerView.startAnimating()

        tableFooterView = footerView
    }

    func hideLoadingFooter() {
        let tableContentSufficentlyTall = contentSize.height > frame.size.height
        let atBottomOfTable = contentOffset.y >= contentSize.height - frame.size.height
        if atBottomOfTable && tableContentSufficentlyTall {
            UIView.animate(withDuration: 0.2, animations: {
                self.contentOffset.y = self.contentOffset.y - 50
            }, completion: { _ in
                self.tableFooterView = UIView()
            })
        } else {
            tableFooterView = UIView()
        }
    }

    var isLoadingFooterShowing: Bool {
        return tableFooterView is UIActivityIndicatorView
    }
}
