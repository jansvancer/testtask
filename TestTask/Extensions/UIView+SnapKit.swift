//
//  UIView+SnapKit.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit
import SnapKit

extension UIView {

    var safeArea: ConstraintBasicAttributesDSL {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp
        }

        return self.snp
    }
}
