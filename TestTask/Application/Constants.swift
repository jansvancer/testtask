//
//  Constants.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit

typealias L = R.string.localizable

struct Constants {

    // MARK: - API

    struct API {
        private static let apiHostname = "https://service.bmf.gv.at"

        static let baseURLString = "\(apiHostname)"
    }

    // MARK: - Keychain keys

    enum KeychainKeys: String {
        case authenticationToken
    }

    // MARK: - UserDefaults keys

    enum UserDefaults: String {
        case dummyKey
    }

    // MARK: - Decoder

    static let jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(Formatters.dateApiFormatter)

        return decoder
    }()

    static let jsonEncoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(Formatters.dateApiFormatter)

        return encoder
    }()
}
