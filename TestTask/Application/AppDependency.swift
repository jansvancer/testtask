//
//  AppDependency.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit

typealias AllDependencies = HasDummyManager & HasOfficeService

protocol HasDummyManager {}

protocol HasOfficeService {
    var officeService: OfficeService { get }
}

struct AppDependency: AllDependencies {

    let realmManager: RealmManager
    let apiService: ApiService
    let officeService: OfficeService

    init(application: UIApplication) {
        realmManager = RealmManager()
        apiService = ApiService()

        officeService = OfficeService(apiService: apiService, realmManager: realmManager)
    }
}
