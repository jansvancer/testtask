//
//  Appearance.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit

struct Appearance {

    // MARK: - Global

    static func setGlobalAppearance() {
        UINavigationBar.appearance().tap {
            $0.prefersLargeTitles = false
        }
    }

    // MARK: - Fonts

    static func font(ofSize size: CGFloat, weight: UIFont.Weight = .regular) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: weight)
    }

    // MARK: - Colors

    struct Colors {
        static let mainTint = UIColor("#007AFF")
        static let alertRed = UIColor("#EA1E1E")

        static let whiteText = UIColor("#FFFFFF")
        static let darkText = UIColor("#151A29")
        static let subdarkText = UIColor("#474747")

        static let whiteBackground = UIColor("#FFFFFF")
        static let separatorBackground = UIColor("#EBEBEF")
    }

    // MARK: - Styles

    struct Styles {
        static let mainView = UIViewStyle<UIView> {
            $0.backgroundColor = Colors.whiteBackground
        }

        static let oneLineAdjustableLabel = UIViewStyle<UILabel> {
            $0.numberOfLines = 1
            $0.adjustsFontSizeToFitWidth = true
            $0.minimumScaleFactor = 0.7
        }

        static let tableView = UIViewStyle<UITableView> {
            $0.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude))
            $0.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude))
            $0.backgroundColor = .clear
            $0.separatorStyle = .none
            $0.contentInsetAdjustmentBehavior = .never
        }

        static let separatorView = UIViewStyle<UIView> {
            $0.backgroundColor = Colors.separatorBackground
        }
    }
}
