//
//  AppCoordinator.swift
//  TestTask
//
//  Created by Jan Švancer on 04/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit
import RxSwift

final class AppCoordinator: BaseCoordinator<Void> {

    private let window: UIWindow
    private let dependencies: AppDependency

    init(window: UIWindow, application: UIApplication) {
        self.window = window
        self.dependencies = AppDependency(application: application)
    }

    override func start() -> Observable<Void> {
        coordinateToRoot()
        return Observable.never()
    }

    // Recursive method that will restart a child coordinator after completion.
    private func coordinateToRoot() {
        showOfficesList()
            .subscribe(onNext: { [weak self] _ in
                self?.window.rootViewController = nil
                self?.coordinateToRoot()
            })
            .disposed(by: disposeBag)
    }

    private func showOfficesList() -> Observable<Void> {
        let coordinator = OfficesListCoordinator(window: window, dependencies: dependencies)
        return coordinate(to: coordinator)
    }

}
