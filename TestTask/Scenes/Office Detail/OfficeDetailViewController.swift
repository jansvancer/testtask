//
//  OfficeDetailViewController.swift
//  TestTask
//
//  Created by Jan Švancer on 17/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher

final class OfficeDetailViewController: RxViewController, ViewModelAttaching {

    var viewModel: OfficeDetailViewModel!
    var bindings: OfficeDetailViewModel.Bindings {
        return OfficeDetailViewModel.Bindings(
            didMoveToParent: rx.sentMessage(#selector(UIViewController.didMove)).asDriverOnErrorJustReturnNil(),
            mapTap: mapButton.rx.tap.asDriver()
        )
    }

    // MARK: - UI

    private let scrollView = UIScrollView()
    private let contentView = UIView()
    private let contentStackView = UIStackView()
    private let nameLabel = UILabel()
    private let imageView = UIImageView()
    private let addressView = TitleMessageView()
    private let openingHoursView = TitleMessageView()
    private let phoneView = TitleMessageView()
    private let mapButtonView = UIView()
    private let mapSeparatorView = UIView()
    private let mapButton = UIButton()

    // MARK: - Properties

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func setupUI() {
        Appearance.Styles.mainView.apply(to: view)
        Styles.contentStackView.apply(to: contentStackView)
        Styles.nameLabel.apply(to: nameLabel)
        Styles.imageView.apply(to: imageView)
        Appearance.Styles.separatorView.apply(to: mapSeparatorView)
        Styles.mapButton.apply(to: mapButton)

        mapButton.setTitle(tr(L.officeDetailMapButton), for: .normal)
    }

    func addSubviews() {
        contentStackView.addArrangedSubview(nameLabel)
        contentStackView.addArrangedSubview(imageView)
        contentStackView.addArrangedSubview(addressView)
        contentStackView.addArrangedSubview(openingHoursView)
        contentStackView.addArrangedSubview(phoneView)

        mapButtonView.addSubview(mapSeparatorView)
        mapButtonView.addSubview(mapButton)

        contentView.addSubview(contentStackView)
        scrollView.addSubview(contentView)
        view.addSubview(scrollView)
        view.addSubview(mapButtonView)
    }

    func layout() {
        scrollView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.leading.equalTo(view.safeArea.leading)
            make.trailing.equalTo(view.safeArea.trailing)
        }

        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview().priority(250)
        }

        contentStackView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(16)
            make.bottom.equalToSuperview().offset(-16)
        }

        imageView.snp.makeConstraints { make in
            make.height.equalTo(200)
        }

        mapButtonView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(scrollView.snp.bottom)
            make.bottom.equalTo(view.safeArea.bottom)
        }

        mapSeparatorView.snp.makeConstraints { make in
            make.height.equalTo(1)
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview()
        }

        mapButton.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(16)
            make.bottom.equalToSuperview().offset(-16)
        }
    }

    override func setupVisibleBindings(for visibleDisposeBag: DisposeBag) {
        super.setupVisibleBindings(for: visibleDisposeBag)

        viewModel.nameLabel
            .drive(nameLabel.rx.text)
            .disposed(by: visibleDisposeBag)

        viewModel.imageUrl
            .drive(onNext: { [unowned self] in
                self.imageView.kf.setImage(with: $0)
            })
            .disposed(by: visibleDisposeBag)

        viewModel.addressView
            .drive(addressView.rx.update)
            .disposed(by: visibleDisposeBag)

        viewModel.openingHoursView
            .drive(openingHoursView.rx.update)
            .disposed(by: visibleDisposeBag)

        viewModel.phoneView
            .drive(phoneView.rx.update)
            .disposed(by: visibleDisposeBag)
    }
}

// MARK: - Styles

extension OfficeDetailViewController {
    struct Styles {
        static let contentStackView = UIViewStyle<UIStackView> {
            $0.axis = .vertical
            $0.alignment = .fill
            $0.distribution = .fill
            $0.spacing = 16
        }

        static let nameLabel = UIViewStyle<UILabel> {
            $0.font = Appearance.font(ofSize: 16, weight: .semibold)
            $0.textColor = Appearance.Colors.darkText
            $0.numberOfLines = 0
        }

        static let imageView = UIViewStyle<UIImageView> {
            $0.contentMode = .scaleAspectFill
            $0.layer.masksToBounds = true
            $0.backgroundColor = Appearance.Colors.separatorBackground
        }

        static let mapButton = UIViewStyle<UIButton> {
            $0.setTitleColor(Appearance.Colors.whiteText, for: .normal)
            $0.titleLabel?.font = Appearance.font(ofSize: 16, weight: .bold)
            $0.backgroundColor = Appearance.Colors.mainTint
            $0.layer.cornerRadius = 4
            $0.layer.masksToBounds = true
        }
    }
}

// MARK: - Create

extension OfficeDetailViewController {
    static func create() -> OfficeDetailViewController {
        return OfficeDetailViewController()
    }
}
