//
//  OfficeDetailCoordinator.swift
//  TestTask
//
//  Created by Jan Švancer on 17/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit
import RxSwift

final class OfficeDetailCoordinator: BaseCoordinator<PushCoordinationResult<Void>> {

    typealias Dependencies = AllDependencies

    private let navigationController: UINavigationController
    private let dependencies: Dependencies
    private let officeId: String

    init(navigationController: UINavigationController, dependencies: Dependencies, officeId: String) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.officeId = officeId
    }

    override func start() -> Observable<CoordinationResult> {
        let viewController = OfficeDetailViewController.create()
        let viewModel = viewController.attach(wrapper: ViewModelWrapper<OfficeDetailViewModel>(OfficeDetailViewModel.Dependency(
            officeService: dependencies.officeService,
            officeId: officeId
        )))

        navigationController.pushViewController(viewController, animated: true)

        viewModel.showMap
            .asObservable()
            .flatMap { [weak self] officeId -> Observable<PushCoordinationResult<Void>> in
                guard let self = self else {
                    return .empty()
                }

                return self.showOfficeMap(officeId: officeId)
            }
            .subscribe()
            .disposed(by: disposeBag)

        let popped = viewModel.viewDismissed
            .asObservable()
            .map { _ in PushCoordinationResult<Void>.popped }

        return Observable.merge(popped)
            .take(1)
            .do(onNext: { [weak self] in
                if case .dismiss = $0 {
                    self?.navigationController.popViewController(animated: true)
                }
            })
    }

    private func showOfficeMap(officeId: String) -> Observable<PushCoordinationResult<Void>> {
        let coordinator = OfficeMapCoordinator(navigationController: navigationController,
                                               dependencies: dependencies,
                                               officeId: officeId)
        return coordinate(to: coordinator)
    }
}
