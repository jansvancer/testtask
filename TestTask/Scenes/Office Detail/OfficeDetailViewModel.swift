//
//  OfficeDetailViewModel.swift
//  TestTask
//
//  Created by Jan Švancer on 17/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class OfficeDetailViewModel: ViewModelType {

    // MARK: - ViewModelType

    struct Dependency {
        let officeService: OfficeService
        let officeId: String
    }

    struct Bindings {
        let didMoveToParent: Driver<[Any]?>
        let mapTap: Driver<Void>!
    }

    // MARK: - Variables

    private(set) var nameLabel: Driver<String?>!
    private(set) var imageUrl: Driver<URL>!
    private(set) var addressView: Driver<TitleMessageView.Data>!
    private(set) var openingHoursView: Driver<TitleMessageView.Data>!
    private(set) var phoneView: Driver<TitleMessageView.Data>!

    // MARK: - Actions

    private(set) var viewDismissed: Driver<Void>!
    private(set) var showMap: Driver<String>!

    // MARK: - Initialization

    init(dependency: Dependency, bindings: Bindings) {
        setupDataUpdates(dependency: dependency, bindings: bindings)
        setupActions(dependency: dependency, bindings: bindings)
    }

    private func setupDataUpdates(dependency: Dependency, bindings: Bindings) {
        let office = dependency.officeService.office(for: dependency.officeId)
            .asDriverFilterNil()

        nameLabel = office
            .map { $0.name }

        imageUrl = office
            .map { $0.imageURL }
            .map { URL(string: $0) }
            .filterNil()

        addressView = office
            .map {
                let address = $0.zip + " " + $0.city + ", " + $0.street
                return TitleMessageView.Data(title: tr(L.officeDetailAddressTitle), message: address)
            }

        openingHoursView = office
            .map { TitleMessageView.Data(title: tr(L.officeDetailOpeningHoursTitle), message: $0.openingHours) }

        phoneView = office
            .map { TitleMessageView.Data(title: tr(L.officeDetailPhoneTitle), message: $0.phone) }
    }

    private func setupActions(dependency: Dependency, bindings: Bindings) {
        viewDismissed = bindings.didMoveToParent
            .filter { ($0?.first as? NSNull) != nil }
            .asVoid

        showMap = bindings.mapTap
            .map { dependency.officeId }
    }
}
