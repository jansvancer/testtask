//
//  OfficesListCoordinator.swift
//  TestTask
//
//  Created by Jan Švancer on 17/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit
import RxSwift

final class OfficesListCoordinator: BaseCoordinator<Void> {

    typealias Dependencies = AllDependencies

    private let window: UIWindow
    private let dependencies: Dependencies

    init(window: UIWindow, dependencies: Dependencies) {
        self.window = window
        self.dependencies = dependencies
    }

    override func start() -> Observable<CoordinationResult> {
        let viewController = OfficesListViewController.create()
        let navigationController = SwipeNavigationController(rootViewController: viewController)
        let viewModel = viewController.attach(wrapper: ViewModelWrapper<OfficesListViewModel>(dependencies))

        window.tap {
            $0.rootViewController = navigationController
            $0.makeKeyAndVisible()
        }

        viewModel.showOfficeDetail
            .asObservable()
            .flatMap { [weak navigationController, weak self] officeId -> Observable<PushCoordinationResult<Void>> in
                guard let navigationController = navigationController, let self = self else {
                    return .empty()
                }

                return self.showOfficeDetail(navigationController: navigationController, officeId: officeId)
            }
            .subscribe()
            .disposed(by: disposeBag)

        return Observable.never()
    }

    private func showOfficeDetail(navigationController: UINavigationController, officeId: String) -> Observable<PushCoordinationResult<Void>> {
        let coordinator = OfficeDetailCoordinator(navigationController: navigationController,
                                                  dependencies: dependencies,
                                                  officeId: officeId)
        return coordinate(to: coordinator)
    }
}
