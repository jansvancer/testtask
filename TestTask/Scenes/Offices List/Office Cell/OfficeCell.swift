//
//  OfficeCell.swift
//  TestTask
//
//  Created by Jan Švancer on 17/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit

final class OfficeCell: UITableViewCell, ReusableView {

    // MARK: - UI

    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()
    private let separatorView = UIView()

    // MARK: - Initialization

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupUI()
    }

    private func setupUI() {
        selectionStyle = .none

        Styles.titleLabel.apply(to: titleLabel)
        Styles.subtitleLabel.apply(to: subtitleLabel)
        Appearance.Styles.separatorView.apply(to: separatorView)

        addSubviews()
        layout()
    }

    private func addSubviews() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(separatorView)
    }

    private func layout() {
        titleLabel.setContentHuggingPriority(UILayoutPriority(251), for: .vertical)

        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(16)
        }

        subtitleLabel.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.leading)
            make.trailing.equalTo(titleLabel.snp.trailing)
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.bottom.equalToSuperview().offset(-16)
        }

        separatorView.snp.makeConstraints { make in
            make.height.equalTo(1)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
}

// MARK: - Update

extension OfficeCell {
    func update(with viewModel: OfficeCellViewModel) {
        titleLabel.text = viewModel.titleLabel
        subtitleLabel.text = viewModel.subtitleLabel
    }
}

// MARK: - Styles

extension OfficeCell {
    struct Styles {
        static let titleLabel = UIViewStyle<UILabel> {
            $0.textColor = Appearance.Colors.darkText
            $0.font = Appearance.font(ofSize: 16, weight: .semibold)
            $0.numberOfLines = 0
        }

        static let subtitleLabel = UIViewStyle<UILabel> {
            $0.textColor = Appearance.Colors.subdarkText
            $0.font = Appearance.font(ofSize: 12, weight: .regular)
            $0.numberOfLines = 0
        }
    }
}
