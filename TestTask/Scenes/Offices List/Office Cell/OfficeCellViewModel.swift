//
//  OfficeCellViewModel.swift
//  TestTask
//
//  Created by Jan Švancer on 17/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit

struct OfficeCellViewModel {
    let office: FinanceOffice

    // MARK: - Computed Properties

    var titleLabel: String? {
        return office.name
    }

    var subtitleLabel: String? {
        return office.zip + " " + office.city + ", " + office.street
    }
}
