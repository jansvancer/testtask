//
//  OfficesListViewModel.swift
//  TestTask
//
//  Created by Jan Švancer on 17/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class OfficesListViewModel: ViewModelType {

    // MARK: - ViewModelType

    typealias Dependency = HasOfficeService

    struct Bindings {
        let viewDidLoad: Driver<Void>
        let pullToRefresh: Driver<Void>
        let tableTap: Driver<IndexPath>
    }

    // MARK: - Variables

    let loading: Driver<Bool>
    let refreshing: Driver<Bool>
    let errors: Driver<Error>

    let reloadTableView = PublishSubject<Void>()
    private var cells: [CellType] = []

    private let activityIndicator = RxActivityIndicator()
    private let pullToRefreshIndicator = RxActivityIndicator()
    private let errorTracker = RxErrorTracker()

    // MARK: - Actions

    private(set) var showOfficeDetail: Driver<String>!

    // MARK: - Initialization

    let disposeBag = DisposeBag()

    init(dependency: Dependency, bindings: Bindings) {
        loading = activityIndicator.asDriver()
        refreshing = pullToRefreshIndicator.asDriver()
        errors = errorTracker.asDriver()

        setupDataUpdates(dependency: dependency, bindings: bindings)
        setupBackgroundUpdates(dependency: dependency, bindings: bindings)
        setupActions(dependency: dependency, bindings: bindings)
    }

    private func setupDataUpdates(dependency: Dependency, bindings: Bindings) {
        dependency.officeService.listAllOffices()
            .map { [unowned self] offices in
                self.cells = self.generateCells(offices: offices)
            }
            .asVoid
            .bind(to: reloadTableView)
            .disposed(by: disposeBag)
    }

    private func setupBackgroundUpdates(dependency: Dependency, bindings: Bindings) {
        bindings.viewDidLoad
            .flatMapLatest { [unowned self] in
                dependency.officeService.updateOffices()
                    .trackActivity(self.activityIndicator)
                    .trackError(self.errorTracker)
                    .asDriver(onErrorJustReturn: [])
            }
            .drive()
            .disposed(by: disposeBag)

        bindings.pullToRefresh
            .flatMapLatest { [unowned self] in
                dependency.officeService.updateOffices()
                    .trackActivity(self.pullToRefreshIndicator)
                    .trackError(self.errorTracker)
                    .asDriver(onErrorJustReturn: [])
            }
            .drive()
            .disposed(by: disposeBag)
    }

    private func setupActions(dependency: Dependency, bindings: Bindings) {
        showOfficeDetail = bindings.tableTap
            .map { [weak self] indexPath -> String? in
                guard let self = self else {
                    return nil
                }

                let cellType = self.cellTypeForRow(at: indexPath)

                switch cellType {
                case .office(let viewModel)?:
                    return viewModel.office.id
                default:
                    return nil
                }
            }
            .filterNil()
    }

    private func generateCells(offices: [FinanceOffice]) -> [CellType] {
        return offices.map { office in
            CellType.office(OfficeCellViewModel(office: office))
        }
    }
}

// MARK: - TableView

extension OfficesListViewModel {
    var numberOfSections: Int {
        return 1
    }

    func numberOfRows(in section: Int) -> Int {
        return cells.count
    }

    func cellTypeForRow(at indexPath: IndexPath) -> CellType? {
        guard indexPath.row < cells.count else {
            return nil
        }

        return cells[indexPath.row]
    }

    func heightForRow(at indexPath: IndexPath) -> CGFloat {
        guard let cellType = cellTypeForRow(at: indexPath) else {
            return 0
        }

        switch cellType {
        case .office:
            return UITableView.automaticDimension
        }
    }

    func estimatedHeightForRow(at indexPath: IndexPath) -> CGFloat {
        guard let cellType = cellTypeForRow(at: indexPath) else {
            return 0
        }

        switch cellType {
        case .office:
            return 68
        }
    }
}

// MARK: - CellType

extension OfficesListViewModel {
    enum CellType {
        case office(OfficeCellViewModel)
    }
}
