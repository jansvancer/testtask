//
//  OfficesListViewController.swift
//  TestTask
//
//  Created by Jan Švancer on 17/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class OfficesListViewController: RxViewController, ViewModelAttaching {

    var viewModel: OfficesListViewModel!
    var bindings: OfficesListViewModel.Bindings {
        return OfficesListViewModel.Bindings(
            viewDidLoad: rx.sentMessage(#selector(UIViewController.viewDidLoad)).asVoid.asDriverFilterNil(),
            pullToRefresh: refreshControl.rx.controlEvent(.valueChanged).asDriverFilterNil(),
            tableTap: tableView.rx.itemSelected.asDriver()
        )
    }

    // MARK: - UI

    private let tableView = UITableView()
    private let refreshControl = UIRefreshControl()

    // MARK: - Properties

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func setupUI() {
        Appearance.Styles.mainView.apply(to: view)
        Appearance.Styles.tableView.apply(to: tableView)

        tableView.tap {
            $0.register(OfficeCell.self, forCellReuseIdentifier: OfficeCell.reuseIdentifier)
            $0.refreshControl = refreshControl
        }
    }

    func addSubviews() {
        view.addSubview(tableView)
    }

    func layout() {
        tableView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(view.safeArea.top)
            make.bottom.equalTo(view.safeArea.bottom)
        }
    }

    func setupNavigationBar() {
        title = tr(L.officesListTitle)
    }

    override func setupVisibleBindings(for visibleDisposeBag: DisposeBag) {
        super.setupVisibleBindings(for: visibleDisposeBag)

        viewModel.loading
            .drive(tableView.rx.footerActivityIndicator)
            .disposed(by: visibleDisposeBag)

        viewModel.refreshing
            .drive(refreshControl.rx.isRefreshing)
            .disposed(by: visibleDisposeBag)

        viewModel.errors
            .drive(self.rx.errors)
            .disposed(by: visibleDisposeBag)

        tableView.rx
            .setDataSource(self)
            .disposed(by: visibleDisposeBag)

        tableView.rx
            .setDelegate(self)
            .disposed(by: visibleDisposeBag)

        viewModel.reloadTableView
            .asDriverFilterNil()
            .drive(tableView.rx.reloadData)
            .disposed(by: visibleDisposeBag)
    }
}

// MARK: - TableView

extension OfficesListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellType = viewModel.cellTypeForRow(at: indexPath) else {
            return UITableViewCell()
        }

        switch cellType {
        case .office(let viewModel):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: OfficeCell.reuseIdentifier,
                                                           for: indexPath) as? OfficeCell else {
                                                            return UITableViewCell()
            }

            cell.update(with: viewModel)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.heightForRow(at: indexPath)
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.estimatedHeightForRow(at: indexPath)
    }
}

// MARK: - Styles

extension OfficesListViewController {
    struct Styles {
    }
}

// MARK: - Create

extension OfficesListViewController {
    static func create() -> OfficesListViewController {
        return OfficesListViewController()
    }
}
