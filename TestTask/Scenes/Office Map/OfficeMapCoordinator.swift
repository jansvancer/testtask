//
//  OfficeMapCoordinator.swift
//  TestTask
//
//  Created by Jan Švancer on 17/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit
import RxSwift

final class OfficeMapCoordinator: BaseCoordinator<PushCoordinationResult<Void>> {

    typealias Dependencies = AllDependencies

    private let navigationController: UINavigationController
    private let dependencies: Dependencies
    private let officeId: String

    init(navigationController: UINavigationController, dependencies: Dependencies, officeId: String) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.officeId = officeId
    }

    override func start() -> Observable<CoordinationResult> {
        let viewController = OfficeMapViewController.create()
        let viewModel = viewController.attach(wrapper: ViewModelWrapper<OfficeMapViewModel>(OfficeMapViewModel.Dependency(
            officeService: dependencies.officeService,
            officeId: officeId
        )))

        navigationController.pushViewController(viewController, animated: true)

        let popped = viewModel.viewDismissed
            .asObservable()
            .map { _ in PushCoordinationResult<Void>.popped }

        return Observable.merge(popped)
            .take(1)
            .do(onNext: { [weak self] in
                if case .dismiss = $0 {
                    self?.navigationController.popViewController(animated: true)
                }
            })
    }
}
