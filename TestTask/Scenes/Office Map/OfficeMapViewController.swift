//
//  OfficeMapViewController.swift
//  TestTask
//
//  Created by Jan Švancer on 17/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MapKit

final class OfficeMapViewController: RxViewController, ViewModelAttaching {

    var viewModel: OfficeMapViewModel!
    var bindings: OfficeMapViewModel.Bindings {
        return OfficeMapViewModel.Bindings(
            didMoveToParent: rx.sentMessage(#selector(UIViewController.didMove)).asDriverOnErrorJustReturnNil()
        )
    }

    // MARK: - UI

    private let mapView = MKMapView()

    // MARK: - Properties

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        mapView.removeAnnotations(mapView.annotations)
    }

    func setupUI() {
        Appearance.Styles.mainView.apply(to: view)
    }

    func addSubviews() {
        view.addSubview(mapView)
    }

    func layout() {
        mapView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(view.safeArea.top)
            make.bottom.equalTo(view.safeArea.bottom)
        }
    }

    func setupNavigationBar() {
        title = tr(L.officeMapTitle)
    }

    override func setupVisibleBindings(for visibleDisposeBag: DisposeBag) {
        super.setupVisibleBindings(for: visibleDisposeBag)

        viewModel.officeCoordinate
            .drive(onNext: { [unowned self] coordinate in
                self.addAnnotation(coordinate: coordinate)
            })
            .disposed(by: visibleDisposeBag)
    }

    private func addAnnotation(coordinate: CLLocationCoordinate2D) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate

        mapView.addAnnotation(annotation)

        let span = MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5)
        let region = MKCoordinateRegion(center: annotation.coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
}

// MARK: - Styles

extension OfficeMapViewController {
    struct Styles {
    }
}

// MARK: - Create

extension OfficeMapViewController {
    static func create() -> OfficeMapViewController {
        return OfficeMapViewController()
    }
}
