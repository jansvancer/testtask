//
//  OfficeMapViewModel.swift
//  TestTask
//
//  Created by Jan Švancer on 17/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MapKit

final class OfficeMapViewModel: ViewModelType {

    // MARK: - ViewModelType

    struct Dependency {
        let officeService: OfficeService
        let officeId: String
    }

    struct Bindings {
        let didMoveToParent: Driver<[Any]?>
    }

    // MARK: - Variables

    private(set) var officeCoordinate: Driver<CLLocationCoordinate2D>!

    // MARK: - Actions

    private(set) var viewDismissed: Driver<Void>!

    // MARK: - Initialization

    init(dependency: Dependency, bindings: Bindings) {
        setupDataUpdates(dependency: dependency, bindings: bindings)
        setupActions(dependency: dependency, bindings: bindings)
    }

    private func setupDataUpdates(dependency: Dependency, bindings: Bindings) {
        let office = dependency.officeService.office(for: dependency.officeId)
            .asDriverFilterNil()

        officeCoordinate = office
            .map { (Double($0.latitude), Double($0.longitude)) }
            .filter { $0.0 != nil && $0.1 != nil }
            .map { CLLocationCoordinate2D(latitude: $0.0!, longitude: $0.1!) }
    }

    private func setupActions(dependency: Dependency, bindings: Bindings) {
        viewDismissed = bindings.didMoveToParent
            .filter { ($0?.first as? NSNull) != nil }
            .asVoid
    }
}
