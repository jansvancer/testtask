//
//  TitleMessageView.swift
//  TestTask
//
//  Created by Jan Švancer on 17/09/2019.
//  Copyright © 2019 Jan Švancer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class TitleMessageView: UIView {

    typealias Data = (title: String?, message: String?)

    // MARK: - UI

    private let titleLabel = UILabel()
    private let messageLabel = UILabel()

    // MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupUI()
    }

    private func setupUI() {
        Styles.titleLabel.apply(to: titleLabel)
        Styles.messageLabel.apply(to: messageLabel)

        addSubviews()
        layout()
    }

    private func addSubviews() {
        addSubview(titleLabel)
        addSubview(messageLabel)
    }

    private func layout() {
        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.top.equalToSuperview()
        }

        messageLabel.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.leading)
            make.trailing.equalTo(titleLabel.snp.trailing)
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.bottom.equalToSuperview()
        }
    }
}

// MARK: - Update

extension TitleMessageView {
    func update(with data: Data) {
        titleLabel.text = data.title
        messageLabel.text = data.message
    }
}

// MARK: - Reactive

extension Reactive where Base: TitleMessageView {
    var update: Binder<TitleMessageView.Data> {
        return Binder(self.base) { view, data in
            view.update(with: data)
        }
    }
}

// MARK: - Styles

extension TitleMessageView {
    struct Styles {
        static let titleLabel = UIViewStyle<UILabel> {
            $0.textColor = Appearance.Colors.darkText
            $0.font = Appearance.font(ofSize: 14, weight: .semibold)
            $0.numberOfLines = 0
        }

        static let messageLabel = UIViewStyle<UILabel> {
            $0.textColor = Appearance.Colors.subdarkText
            $0.font = Appearance.font(ofSize: 12, weight: .regular)
            $0.numberOfLines = 0
        }
    }
}
